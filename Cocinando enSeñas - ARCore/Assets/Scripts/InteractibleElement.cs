﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractibleElement : MonoBehaviour {
    public virtual void Action() { Debug.LogError("Parent action"); }
}
