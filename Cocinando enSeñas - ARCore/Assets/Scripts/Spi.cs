﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spi : InteractibleElement {
    public int step = 0;

    private Animator refAnimator;

    private void Awake()
    {
        refAnimator = this.GetComponent<Animator>();

        LoadAnimator(0);
    }

    public override void Action()
    {
        refAnimator.SetTrigger("play");
        refAnimator.SetInteger("step",this.step);
        this.step++;
    }

    public void LoadAnimator(int step)
    {
        string animatorName = "";
        this.step = 0;

        switch (step)
        {
            case 0: animatorName = "Anim_Paso0_Presentacion";
                    break;
            case 1: animatorName = "Anim_Paso1_MaterialesLista";
                    break;
            case 2: animatorName = "Anim_Paso2_MolerCarne";
                    break;
            case 3: animatorName = "Anim_Paso3_Deshuesar";
                    break;
            case 4: animatorName = "Anim_Paso4_EscaldarVerduras";
                    break;
            case 5: animatorName = "Anim_Paso5_MigaPan";
                    break;
            case 6: animatorName = "Anim_Paso6_Mezclar";
                    break;
            case 7: animatorName = "Anim_Paso7_RellenarCocinarPollo";
                    break;
            case 8: animatorName = "Anim_Paso8_DorarPollo";
                    break;
        }

        refAnimator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("3D/Personaje/Spi/Animators/" + animatorName);        
    }
}
