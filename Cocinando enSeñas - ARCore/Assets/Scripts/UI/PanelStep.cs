﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelStep : MonoBehaviour
{
    private Image           icon;
    private RectTransform   iconRectTransform;
    private Text            iconText;
    private Text            stepText;
    private Image           barra1;
    private Image           barra2;

    private Color       colorAzul = new Color(0.153f, 0.768f, 0.886f);
    private Transform   refTransform = null;

    private void Awake()
    {
        refTransform = this.transform;
        icon                = refTransform.GetChild(0).GetComponent<Image>();
        iconRectTransform   = refTransform.GetChild(0).GetComponent<RectTransform>();
        iconText            = refTransform.GetChild(0).GetChild(0).GetComponent<Text>();
        barra1              = refTransform.GetChild(1).GetComponent<Image>();
        barra2              = refTransform.GetChild(2).GetComponent<Image>();
        stepText            = refTransform.GetChild(3).GetComponent<Text>();
    }

    public void ChangeState(bool enable)
    {
       // Debug.LogError("Entrando al evento");

        if (enable)
        {
            //Cambiar el tamaño y color de elementos Azul
            ChangeIconColor(colorAzul);
            ChangeIconTextColor(Color.white);
            ChangeIconSizeAndPosition(50.6f, 25.3f);
            
        }
        else
        {
            //Cambiar el tamaño y color de elementos Blanco
            ChangeIconColor(Color.white);
            ChangeIconTextColor(Color.gray);
            ChangeIconSizeAndPosition(35.0f, 20.0f);
           
        }
    }

    private void ChangeIconColor(Color newColor)
    {
        icon.color = newColor;
        barra1.color = newColor;
        barra2.color = newColor;
        stepText.color = newColor;
    }

    private void ChangeIconTextColor(Color newTextColor)
    {
        iconText.color = newTextColor;
    }

    private void ChangeIconSizeAndPosition(float size, float yPos)
    {
        iconRectTransform.localPosition = new Vector3(0.0f, yPos, 0.0f);
        iconRectTransform.sizeDelta = new Vector2(size, size);
    }
}
