﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.Examples.HelloAR;

public class UiController : MonoBehaviour
{ 
    private bool menuMostrado = true;

    public Animator refAnimatorPnlSteps;
    public Animator refAnimatorPnlAyuda;

    public Image img_Menu;

    public Canvas canvasContenedorAyudas;

    private Image img_Ayuda;
    private Text txt_Ayuda;
    private GameObject refSpi = null;

    
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Menu))
        {
            Application.Quit();
        }
    }
    private void CambiarIconoBtnMenu(bool mostrado)
    {

        if (mostrado)
        {
            img_Menu.sprite = Resources.Load<Sprite>("2D/UI/Imagenes/Simbolo X");
        }
        else
        {
            img_Menu.sprite = Resources.Load<Sprite>("2D/UI/Imagenes/Barra");
        }

    }

    public void MostrarOcultarMenu()
    {
        refAnimatorPnlSteps.SetBool("mostrar", menuMostrado);
        refAnimatorPnlAyuda.SetBool("mostrar", menuMostrado);

        CambiarIconoBtnMenu(menuMostrado);

        menuMostrado = !menuMostrado;
    }
    public void PnlAyuda( int id)
    {
        if (refSpi == null)
        {
            if (ARController_CocinadoEnSenas.RefSpi != null)
            {
                refSpi = ARController_CocinadoEnSenas.RefSpi;

                Canvas canvasContenedor = null;
                canvasContenedor = refSpi.GetComponentInChildren<Canvas>();

                Debug.Log(refSpi, canvasContenedor);

                txt_Ayuda = canvasContenedor.GetComponent<Text>(); //refSpi.GetComponentInChildren<Text>();
                img_Ayuda = canvasContenedor.GetComponent<Image>(); //refSpi.GetComponentInChildren<Image>();

               
            }
        }

       

        switch (id)
        {
            case 0:
                //Sin ayuda
                txt_Ayuda.gameObject.SetActive(false);
                img_Ayuda.gameObject.SetActive(false);
                break;
            case 1:
                //Ayuda imagenes
                txt_Ayuda.gameObject.SetActive(false);
                img_Ayuda.gameObject.SetActive(true);
                break;
            case 2:
                //Ayuda Texto
                txt_Ayuda.gameObject.SetActive(true);
                img_Ayuda.gameObject.SetActive(false);
                break;
            default:
                txt_Ayuda.gameObject.SetActive(false);
                img_Ayuda.gameObject.SetActive(false);
                break;
        }

        


    }
}
