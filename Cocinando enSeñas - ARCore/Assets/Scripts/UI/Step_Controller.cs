﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.Examples.HelloAR;

public class Step_Controller : MonoBehaviour
{
    private int currentStep = 0;
    private int previousStep = -1;

    private PanelStep[] panelSteps;

    private Spi refSpi;

    private float tamMax = 67.5f;
    private float tamMin = 48f;


    private void Start()
    {
        panelSteps = new PanelStep[this.transform.childCount];

        for (int i = 0; i < this.transform.childCount; i++)
        {
            panelSteps[i] = this.transform.GetChild(i).GetComponent<PanelStep>();
            // Inicialmente solo el primer panel step estará activado
            panelSteps[i].ChangeState( i == 0 );
        }
    }

    public void ChangeStep(int newStep)
    {
        if (newStep != currentStep)
        {
            previousStep = currentStep;
            currentStep = newStep;

            ChangeState(true, currentStep);
            ChangeState(false, previousStep);

            if (refSpi == null)
            {
                if (ARController_CocinadoEnSenas.RefSpi != null)
                {
                    refSpi = ARController_CocinadoEnSenas.RefSpi.GetComponent<Spi>();
                }
                
            }

            refSpi.LoadAnimator(currentStep);
        }
        

    }
    private void ChangeState(bool enable, int step)
    {
        panelSteps[step].ChangeState( enable );
    }
}

